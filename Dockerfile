FROM debian:stable AS build
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends build-essential pkg-config golang-go git ca-certificates

RUN git clone https://github.com/Upload/Up1.git && \
    env GOPATH=/Up1 /usr/bin/go build -o /Up1/server/server /Up1/server/server.go && \
    strip /Up1/server/server

FROM debian:stable

COPY --from=build /Up1/server/server /srv/up1/server/server
COPY --from=build /Up1/server/server.js /srv/up1/server/server.js
COPY client/ /srv/up1/client
COPY static/ /srv/up1/static


ENTRYPOINT ["/srv/up1/server/server", "-config", "/etc/up1/server/server.conf"]


